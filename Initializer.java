import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.table.DefaultTableModel;

import jssc.SerialPortException;

/**
 * Created by aksondhi on 12/13/15.
 */
public class Initializer extends JFrame{
	private static final long serialVersionUID = 1L;

	JScrollPane scrollPane;
	
	String[] columnNames = {"Session", "Status"};	//Column names for the TA session view
	
	private JTextField textField;
	private JLabel lblStatusText;
	private boolean validSession = false;	//Represents whether a valid session has been entered
	private boolean sessionStarted = false;	//Represents whether a ThreeDRendering session has ben created
	private String session ="";	//Represents the name of the session
	static Initializer frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Initializer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Initializer(){
		
		//Creting window
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Mars Rover");
		getContentPane().setLayout(null);
		setBounds(100, 100, 500, 300);
		
		//Student buton
		final JButton btnStudent = new JButton("Student");
		btnStudent.setBounds(190, 101, 117, 29);
		getContentPane().add(btnStudent);
		
		//TA button
		final JButton btnTa = new JButton("TA");
		btnTa.setBounds(190, 142, 117, 29);
		getContentPane().add(btnTa);
		
		final JLabel lblIAmA = new JLabel("I am a...");
		lblIAmA.setBounds(218, 73, 61, 16);
		getContentPane().add(lblIAmA);
		
		//Update button to be used later for the TA view
		final JButton btnUpdate = new JButton("Update");
		btnUpdate.setBounds(364, 243, 117, 29);
		getContentPane().add(btnUpdate);
		
		lblStatusText = new JLabel("");
		lblStatusText.setHorizontalAlignment(SwingConstants.CENTER);
		lblStatusText.setBounds(25, 234, 450, 16);
		getContentPane().add(lblStatusText);
		
		btnUpdate.setVisible(false);
		
		//Student button listener that modifies the view to prompt for session/BAM and port information
		btnStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Hiding buttons
				btnStudent.setVisible(false);
				btnTa.setVisible(false);
				lblIAmA.setVisible(false);
				
				//Setting up new view
				textField = new JTextField();
				textField.setBounds(180, 73, 150, 28);
				getContentPane().add(textField);
				textField.setColumns(10);
				textField.setText("Enter a session name");
				lblStatusText.setText("Enter a session name and BAM#. ex: 'Testing_BAM1'");
				
				//Action listener for textField to validate and move the user through the validation process
				textField.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
						//If no valid session has been entered
						if(!validSession){
							if(validSessionName(textField.getText())){
								validSession = true;
								session = textField.getText();
								setTitle("Mars Rover: "+session);
								lblStatusText.setText("Enter the com port. '/dev/tty.ElementSerial-ElementSe' or 'COM8'");
								textField.setText("");
			
							}else{
								lblStatusText.setText("Enter a VALID session name and BAM#. ex: 'Testing_BAM1'");
							}
						
						//If a valid session has been entered but a session has not been started
						}else if(!sessionStarted){
							//Attempting bluetooth conneciton to given port name
							setTitle("Attempting connection...");
							
							try {
								final Port port = new Port(textField.getText());
								final Thread portThread = new Thread(port);
								portThread.run();
								
								//Waiting for the prot to connect
								while(port.started != 1){
									//If there is an issue, throw an exception
									if(port.started == -1){
										throw new SerialPortException("", "", "");
									}
								}
								
								//All is well, start the GUI and provide it with the port, porthThread and session name 
								lblStatusText.setText("Connected to: " + textField.getText());
								sessionStarted = true;
								Thread thread = new Thread(new ThreeDRendering(port, portThread, session));
								thread.start();

							} catch (SerialPortException e1) {
								//Presents an error to the user
								lblStatusText.setText("Enter a VALID com port. '/dev/tty.ElementSerial-ElementSe' or 'COM8'");
								presentErrorMessage("Unable to Connect to:" + textField.getText());
							}
							setTitle("Mars Rover: "+session);
						}
					}
				});
			}
		});
		
		//Action listener for the update button which updates the table for the TA view
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displaySessions();
			}
		});
		
		//Action listener for the TA button to hide unnecessary elements and present the current session data
		btnTa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStudent.setVisible(false);
				btnTa.setVisible(false);
				lblIAmA.setVisible(false);
				btnUpdate.setVisible(true);
				displaySessions();
				
			}
		});
		
	}
	
	/**
	 * Present the session data from the SQL database
	 */
	private void displaySessions(){
		final ArrayList<String> sessions = new ArrayList<String>();
		ArrayList<String> status = new ArrayList<String>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://mysql.cs.iastate.edu/db319grp39?","dbu319grp39","KKfTExyqx");
			PreparedStatement st = con.prepareStatement("select * from Sessions");
			ResultSet r1 = st.executeQuery();

			DefaultTableModel model = new DefaultTableModel();
			
			while(r1.next()){
				sessions.add(r1.getString("Session"));
				status.add(r1.getString("Status"));
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 6, 490, 235);
		getContentPane().add(scrollPane);
		
		JTable table = new JTable() {
	        private static final long serialVersionUID = 1L;

	        public boolean isCellEditable(int row, int column) {
					Thread thread = new Thread(new SpectatorRender(sessions.get(row)));
					thread.start();
	                return false;               
	        };
	    };
		scrollPane.setViewportView(table);
		
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(columnNames);
		
		for(int row = 0; row < sessions.size(); row++){
			model.addRow(new Object[]{sessions.get(row), status.get(row)});
		}
		table.setModel(model);
	}
	
	/**
	 * Validates that the given sessionName is valid
	 * @param sessionName the session name
	 * @return true if valid
	 */
	private boolean validSessionName(String sessionName){
		Scanner givenScan = new Scanner(sessionName).useDelimiter("_");
		String givenName = "";
		String givenBam = "";
		while(givenScan.hasNext()){
			givenName += givenBam;
			givenBam = givenScan.next();
		}
		System.out.println(givenName);
		System.out.println(givenBam);
		if(!validBam(givenBam)){
			return false;
		}
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://mysql.cs.iastate.edu/db319grp39?","dbu319grp39","KKfTExyqx");
			PreparedStatement st = con.prepareStatement("select * from Sessions");
			ResultSet r1 = st.executeQuery();

			DefaultTableModel model = new DefaultTableModel();
			
			while(r1.next()){
				Scanner scan = new Scanner(r1.getString("Session")).useDelimiter("_");
				String name = "";
				String bam = "";
				while(scan.hasNext()){
					name += bam;
					bam = scan.next();
				}
				System.out.println(name);
				System.out.println(bam);
				if(name.toUpperCase().equals(givenName.toUpperCase()) && bam.toUpperCase().equals(givenBam.toUpperCase())){
					presentErrorMessage("Duplicate Sesion Name");
					return false;
				}
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		return true;
	}
	
	/**
	 * Determines if the string is a valid BAM name and number
	 * @param bam the string to validate
	 * @return true if valid
	 */
	private boolean validBam(String bam){
		String name = "BAM";
		for(int i = 1; i<=12; i++){
			if((name+i).equals(bam.toUpperCase())){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Presents an error message dialog window with the provided message and closes the program if exit is true
	 * @param message	message to present
	 */
	private void presentErrorMessage(String message){
		JOptionPane.showMessageDialog(frame,
				message, "Error",
				JOptionPane.ERROR_MESSAGE);
	}
}
