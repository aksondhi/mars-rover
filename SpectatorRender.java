/**
 * This class runs the developed spectator GUI
 * Created by Arun Sondhi on 11/18/15.
 */

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.*;

import jssc.SerialPortException;

import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import java.util.List;
import java.util.ArrayList;
import com.jme3.scene.Spatial;
import java.sql.*;


public class SpectatorRender extends SimpleApplication implements Runnable{

    protected Node roombaNode = new Node("roombaNode");
    protected boolean processing = false;
    protected Camera cam2;
    protected static Port port;
    private BitmapText status = null;
    private boolean altPerspective = false;

    private float camZoom = 500;
    private String sessionName;
	private Connection con;

    public SpectatorRender (String sessionName){
    	this.sessionName = sessionName;
    	try {
			Class.forName("com.mysql.jdbc.Driver");
	    	con = DriverManager.getConnection("jdbc:mysql://mysql.cs.iastate.edu/db319grp39?","dbu319grp39","KKfTExyqx");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	@Override
	public void run() {
		//Launching the GUI
	    this.start();
	}

    @Override
    public void simpleInitApp() {

        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        status = new BitmapText(guiFont, false);
        status.setSize(guiFont.getCharSet().getRenderedSize());
        guiNode.attachChild(status);

        Cylinder b = new Cylinder(100, 100, 17.25f, 7.5f, true);
        Geometry geom = new Geometry("Box", b);

        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Black);

        geom.setMaterial(mat);
        Node pivot = new Node("pivot");
        pivot.rotate((float) (3.14/2.0),0,0);
        pivot.attachChild(geom);
        roombaNode.attachChild(pivot);

        cam.setLocation(new Vector3f(0,6,0));

        //The floor
        Box aBox = new Box(new Vector3f(0f,-1f,0f), 2000f,1f,2000f);
        Geometry aBoxG = new Geometry("Floor", aBox);
        Material aBoxM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        aBoxM.setColor("Color", ColorRGBA.Gray);
        aBoxG.setMaterial(aBoxM);
        rootNode.attachChild(aBoxG);

        rootNode.attachChild(roombaNode);
        viewPort.setBackgroundColor(ColorRGBA.LightGray);
        cam2 = cam.clone();
        cam2.setLocation(new Vector3f(0,500,0));
        cam2.lookAt(new Vector3f(0,-1,0), new Vector3f(0,0,-1));
        cam2.setViewPort(.3f, .7f, 0.7f, 1f);
        ViewPort viewPort2 = renderManager.createMainView("PiP", cam2);
        viewPort2.setClearFlags(true, true, true);
        viewPort2.attachScene(rootNode);

        initKeys();

    }

    protected boolean reinitalizedInput = false;
    protected int pos = 0;

    /** Custom Keybinding: Map named actions to inputs. */
    private void initKeys() {

        inputManager.addMapping("ToggleView",  new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("IncrementZoom",  new KeyTrigger(KeyInput.KEY_I));
        inputManager.addMapping("DecrementZoom",  new KeyTrigger(KeyInput.KEY_O));

        // Add the names to the action listener.
        inputManager.addListener(analogListener,"IncrementZoom","DecrementZoom");    //For repetitive calls
        inputManager.addListener(actionListener,"ToggleView"); //For single clicks
    }

    @Override
    public void simpleUpdate(float tpf) {
        parseBoulderDatabase();
        parseBoundaries();
        parsePosts();
        if(!altPerspective){
        	parseView();
        }
        parseTrail();
        parseStatusData();

        //Adjusts bird's eye view if not correctly centered
        if(cam2.getLocation().getY() != camZoom){
            cam2.setLocation(cam2.getLocation().setY(camZoom));
        }

        //Disabling some default controls
        if(!reinitalizedInput) {
            inputManager.setCursorVisible(true);
            reinitalizedInput = true;
            setDisplayStatView(false); setDisplayFps(false);
        }
    }

    // ----------
    // Action Listeners
    // ----------
    
    /**
     * Action listener for all key inputs except zoom and executes the corresponding command if the robot is not already busy
     */
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (keyPressed && name.equals("ToggleView")) {
                altPerspective = !altPerspective;
            }
        }
    };

    /**
     * An action listener for the zoom buttons allowing for a fluid zoom in and out.
     */
    private AnalogListener analogListener = new AnalogListener() {
        public void onAnalog(String name, float value, float tpf) {
            if(name.equals("IncrementZoom")){
                incrementZoom(true);
            }
            if(name.equals("DecrementZoom")){
                incrementZoom(false);
            }
        }
    };

    // -----------
    // GUI Updates
    // -----------

    /**
     * Updates the status text
     * @param s String
     */
    private void updateStatus(String s){
        status.setText(s);
        status.setLocalTranslation((float)(cam.getWidth()/2 - status.getLineWidth()*.5), status.getLineHeight()+5, 0);
    }

    /**
     * Increments the zoom height based on whether it's the positive or negative button that was pressed (Max: 1000, Min: 100, Increment: 1)
     * @param positive Boolean of whether to increment in positive or negative direction
     */
    private void incrementZoom(boolean positive){
        if(!positive){
            if(camZoom != 1000){
                camZoom+=1;
            }
        }else{
            if(camZoom != 100){
                camZoom -=1;
            }
        }
    }
    
    // ----------------
    // Database Methods
    // ----------------
    
    /**
     * Parses the boulder table and places new boulders
     */
    private void parseBoulderDatabase(){
    	try{
    		
    		
    		ArrayList<String> objects = getObjectList();
   			Class.forName("com.mysql.jdbc.Driver");

			PreparedStatement st = con.prepareStatement("select * from "+sessionName+"_Boulders");
			ResultSet r1 = st.executeQuery();
			
			while(r1.next()){
				if(!objects.contains(r1.getString("Name"))){
					System.out.println("Placing boulder");
					placeBoulder(r1.getString("Name"), r1.getFloat("x"), r1.getFloat("y"), r1.getFloat("z"), 13f, 5f);
				}
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Places a new boulder
     * @param name object's name
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     * @param height objects height
     * @param radius objects radius
     */
    private void placeBoulder(String name, float x, float y, float z, float radius, float height){
    	Cylinder b = new Cylinder(100, 100, (float) (radius), (float) height*2, true);
        Geometry geom = new Geometry("boulder", b);  // create cube geometry from the shape
        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
        if (radius*2.0 < 10) {
            mat.setColor("Color", ColorRGBA.Green);//Probably a goal post
        } else if (radius*2.0 < 12) {
            mat.setColor("Color", ColorRGBA.Yellow);//Possibly a goal post
        } else {
            mat.setColor("Color", ColorRGBA.Red);   //Not a goal post
        }

        geom.setMaterial(mat);                   // set the cube's material
        Node pivot = new Node(name);
        pivot.attachChild(geom);
        geom.rotate((float) (3.14 / 2.0), 0, 0);
        pivot.move(x, y, z);
        rootNode.attachChild(pivot);
    }
    
    /**
     * Parses boundaries database and places new boundaries
     */
    private void parseBoundaries(){
    	try{
    		ArrayList<String> objects = getObjectList();
   			Class.forName("com.mysql.jdbc.Driver");

			PreparedStatement st = con.prepareStatement("select * from "+sessionName+"_Boundaries");
			ResultSet r1 = st.executeQuery();
			
			while(r1.next()){
				if(!objects.contains(r1.getString("Name"))){
					System.out.println("Placing boundary");
					placeBoundary(r1.getString("Name"), r1.getFloat("x"), r1.getFloat("y"), r1.getFloat("z"));
				}
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Places a boundary object
     * @param name object's name
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     */
    private void placeBoundary(String name, float x, float y, float z){
    	if(name.contains("cliff")){
    		Sphere b = new Sphere(100, 100, 3f);
            Geometry geom = new Geometry("cliff", b);  // create cube geometry from the shape
            Material mat = new Material(assetManager,
                    "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
            mat.setColor("Color", ColorRGBA.Blue);//Probably a goal post

            geom.setMaterial(mat);                   // set the cube's material
            Node pivot = new Node(name);
            pivot.attachChild(geom);
            geom.rotate((float) (3.14 / 2.0), 0, 0);
            pivot.move(x,y,z);
            rootNode.attachChild(pivot);
    	}else{
    		Sphere b = new Sphere(100, 100, 3f);
            Geometry geom = new Geometry("cliff", b);  // create cube geometry from the shape
            Material mat = new Material(assetManager,
                    "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
            mat.setColor("Color", ColorRGBA.Red);//Probably a goal post

            geom.setMaterial(mat);                   // set the cube's material
            Node pivot = new Node(name);
            pivot.attachChild(geom);
            geom.rotate((float) (3.14 / 2.0), 0, 0);
            pivot.move(x,y,z);
            rootNode.attachChild(pivot);
    	}
    }
    
    /**
     * Parses post data and places new posts 
     */
    private void parsePosts(){
    	try{
    		ArrayList<String> objects = getObjectList();
   			Class.forName("com.mysql.jdbc.Driver");

			PreparedStatement st = con.prepareStatement("select * from "+sessionName+"_Posts");
			ResultSet r1 = st.executeQuery();
			
			while(r1.next()){
				if(!objects.contains(r1.getString("Name"))){
					System.out.println("Placing post");
					placePost(r1.getString("Name"), r1.getString("Color"), r1.getFloat("x"), r1.getFloat("y"), r1.getFloat("z"), r1.getFloat("Height"), r1.getFloat("Radius"));
				}
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Places a post
     * @param name object's name
     * @param color object's color
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     * @param height objects height
     * @param radius objects radius
     */
    private void placePost(String name, String color, float x, float y, float z, float height, float radius){
    	
		Cylinder b = new Cylinder(100, 100, (float) (radius), (float) height*2, true);
        Geometry geom = new Geometry("goal", b);  // create cube geometry from the shape
        Material mat;
        if(color.equals("red")){
        	mat = new Material(assetManager,
                    "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
                mat.setColor("Color", ColorRGBA.Red);//Probably a goal post
        }else{
        	mat = new Material(assetManager,
                    "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
                mat.setColor("Color", ColorRGBA.Green);//Probably a goal post
        }
        
        geom.setMaterial(mat);                   // set the cube's material
        Node pivot = new Node(name);
        pivot.attachChild(geom);
        geom.rotate((float) (3.14 / 2.0), 0, 0);
        pivot.move(x,y,z);
        rootNode.attachChild(pivot);
    }
    
    /**
     * Parses trail data and places new trail objects
     */
    private void parseTrail(){
    	try{
    		ArrayList<String> objects = getObjectList();
   			Class.forName("com.mysql.jdbc.Driver");

			PreparedStatement st = con.prepareStatement("select * from "+sessionName+"_Trail");
			ResultSet r1 = st.executeQuery();
			
			while(r1.next()){
				if(!objects.contains(r1.getString("Name"))){
					System.out.println("Placing trail: "+r1.getString("Name"));
					placeTrail(r1.getString("Name"), r1.getString("Color"), r1.getFloat("x"), r1.getFloat("y"), r1.getFloat("z"));
				}
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Places a trail object
     * @param name object's name
     * @param color object's color
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     */
    private void placeTrail(String name, String color,float x, float y, float z){
		Sphere b = new Sphere(100, 100, 3f);
        Geometry geom = new Geometry("cliff", b);  // create cube geometry from the shape
        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
        if(color.equals("orange")){
            mat.setColor("Color", ColorRGBA.Orange);//Probably a goal post
        }else{
            mat.setColor("Color", ColorRGBA.Brown);//Probably a goal post

        }

        geom.setMaterial(mat);                   // set the cube's material
        Node pivot = new Node(name);
        pivot.attachChild(geom);
        geom.rotate((float) (3.14 / 2.0), 0, 0);
        pivot.move(x,y,z);
        rootNode.attachChild(pivot);
    }
    
    /**
     * Parses the view data and set the view
     */
    private void parseView(){
    	try{
   			Class.forName("com.mysql.jdbc.Driver");

			PreparedStatement st = con.prepareStatement("select * from "+sessionName+"_View");
			ResultSet r1 = st.executeQuery();
			
			while(r1.next()){
				if(r1.getString("Name").equals("robot")){
					//System.out.println("Parsing robot");
					roombaNode.move(roombaNode.getWorldTranslation().negate());
					roombaNode.move(r1.getFloat("xPos"), r1.getFloat("yPos"),r1.getFloat("zPos"));
				}else{
					
					Vector3f view = new Vector3f(); //View location
					view.set(r1.getFloat("xPos"), r1.getFloat("yPos"),r1.getFloat("zPos"));
					if(!cam.getLocation().equals(view)){
						cam.setLocation(view);
//						System.out.println("Setting view location");
					}
					Vector3f direction = new Vector3f();	//View direction
					direction.set(r1.getFloat("xVec"), r1.getFloat("yVec"),r1.getFloat("zVec"));
//					System.out.println("Setting view direction");
					cam.setLocation(view);
					cam.lookAtDirection(direction, new Vector3f(0,1,0));
			        cam2.setLocation(cam.getLocation().clone().setY((float)camZoom));
			        cam2.lookAtDirection(new Vector3f(0,-1,0), cam.getDirection().clone());
				}
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Parses the status data and updates the status text
     */
    private void parseStatusData(){
    	try{
   			Class.forName("com.mysql.jdbc.Driver");

			PreparedStatement st = con.prepareStatement("select * from "+sessionName+"_StatusText");
			ResultSet r1 = st.executeQuery();
			
			while(r1.next()){
				updateStatus(r1.getString("Status"));
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Gets a list of all the names of objects in the scene
     * @return
     */
    private ArrayList<String> getObjectList(){
		List<Spatial> children = rootNode.getChildren();
		ArrayList<String> objects = new ArrayList<String>();
        for(int i = 0; i<children.size(); i++) {
        	objects.add(children.get(i).getName());
        }
        return objects;
    }
    
    
}