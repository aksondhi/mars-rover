/**
 * This class runs the developed GUI
 * Created by Arun Sondhi on 11/18/15.
 */

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.*;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import java.util.List;
import java.util.Scanner;
import jssc.SerialPortException;
import java.util.ArrayList;
import com.jme3.scene.Spatial;
import java.sql.*;


public class ThreeDRendering extends SimpleApplication implements Runnable{

    protected Node roombaNode = new Node("roombaNode");
    protected boolean processing = false;
    private static Integer previousDegree = null;
    private static Scanner sensorScan = null;
    private boolean scanning = false;
    private boolean moving = false;
    private boolean turning = false;
    private boolean backing = false;
    private Geometry sG;
    protected Camera cam2;
    protected static Port port;
    private ArrayList<String> scanData = new ArrayList<String>();
    private BitmapText status = null;
    private int turnValue = 10;
    private BitmapText turnText = null;
    private int moveValue = 50;
    private BitmapText moveText = null;
    private float camZoom = 500;
    private BitmapText notification = null;
    private static Thread portThread;
    private String sessionName;
    private boolean completed = false;
    private int cliffNum = 0;
    private int boulderNum = 0;
    private int trailNum = 0;
    private int goalPostNum = 0;
    private int postNum = 0;
    private int boundaryNum = 0;
	private Connection con;

    public ThreeDRendering (Port port, Thread portThread, String sessionName){
    	//Initializing necessary data
    	this.sessionName = sessionName;
    	this.port = port;
    	this.portThread = portThread;
    	
    	//Connection to SQL database
    	try {
			Class.forName("com.mysql.jdbc.Driver");
	    	con = DriverManager.getConnection("jdbc:mysql://mysql.cs.iastate.edu/db319grp39?","dbu319grp39","KKfTExyqx");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
    }
    
	@Override
	public void run() {
		//Launching the GUI
		this.start();
	}

    @Override
    public void simpleInitApp() {
    	
    	//Initializing the objects in 3D space
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        status = new BitmapText(guiFont, false);
        status.setSize(guiFont.getCharSet().getRenderedSize());
        guiNode.attachChild(status);

        turnText = new BitmapText(guiFont, false);
        turnText.setSize(guiFont.getCharSet().getRenderedSize());
        updateTurn();
        guiNode.attachChild(turnText);

        moveText = new BitmapText(guiFont, false);
        moveText.setSize(guiFont.getCharSet().getRenderedSize());
        updateMove();
        guiNode.attachChild(moveText);

        notification = new BitmapText(guiFont, false);
        notification.setSize(guiFont.getCharSet().getRenderedSize());
        guiNode.attachChild(notification);

        Cylinder b = new Cylinder(100, 100, 17.25f, 7.5f, true);
        Geometry geom = new Geometry("Box", b); 

        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Black);

        geom.setMaterial(mat);
        Node pivot = new Node("pivot");
        pivot.rotate((float) (3.14/2.0),0,0);
        pivot.attachChild(geom);
        roombaNode.attachChild(pivot);

        cam.setLocation(new Vector3f(0,6,0));

        Sphere s = new Sphere(100, 100, 0.5f);
        sG = new Geometry("Sphere", s);
        Material sM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        sM.setColor("Color", ColorRGBA.Red);
        sG.setMaterial(sM);
        Node dotNode = new Node("dotNode");
        dotNode.move(0,3.8f,-7.25f);
        dotNode.attachChild(sG);

        roombaNode.attachChild(dotNode);

        //The floor
        Box aBox = new Box(new Vector3f(0f,-1f,0f), 2000f,1f,2000f);
        Geometry aBoxG = new Geometry("Floor", aBox);
        Material aBoxM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        aBoxM.setColor("Color", ColorRGBA.Gray);
        aBoxG.setMaterial(aBoxM);
        rootNode.attachChild(aBoxG);

        rootNode.attachChild(roombaNode);
        viewPort.setBackgroundColor(ColorRGBA.LightGray);
        cam2 = cam.clone();
        cam2.setLocation(new Vector3f(0,500,0));
        cam2.lookAt(new Vector3f(0,-1,0), new Vector3f(0,0,-1));
        cam2.setViewPort(.3f, .7f, 0.7f, 1f);
        ViewPort viewPort2 = renderManager.createMainView("PiP", cam2);
        viewPort2.setClearFlags(true, true, true);
        viewPort2.attachScene(rootNode);

        initKeys();

        Vector3f current = roombaNode.getChild("dotNode").getWorldTranslation().clone().add(cam.getDirection().clone().normalize().mult(10f));
        Line line = new Line(current, current.add(cam.getDirection().clone().normalize().mult(moveValue/10.0f)));
        line.setLineWidth(5);
        Geometry lineG = new Geometry("moveLine", line);
        Material lineM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        lineM.setColor("Color", ColorRGBA.White);
        lineG.setMaterial(lineM);
        rootNode.attachChild(lineG);
        
        insertSessionData("In Progress");
        insertSessionTables();
        insertRobotData();
        insertViewData();
        insertStatusData();

       

    }

    protected boolean reinitalizedInput = false;
    protected int pos = 0;

    /** Custom Keybinding: Map named actions to inputs. */
    private void initKeys() {

        inputManager.addMapping("Forward",  new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Backward",  new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Right",  new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Left",  new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Scan",  new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("Play",  new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("ClosePort",  new KeyTrigger(KeyInput.KEY_ESCAPE));
        inputManager.addMapping("IncrementTurn",  new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addMapping("DecrementTurn",  new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addMapping("IncrementMove",  new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping("DecrementMove",  new KeyTrigger(KeyInput.KEY_DOWN));
        inputManager.addMapping("IncrementZoom",  new KeyTrigger(KeyInput.KEY_I));
        inputManager.addMapping("DecrementZoom",  new KeyTrigger(KeyInput.KEY_O));

        // Add the names to the action listener.
        inputManager.addListener(analogListener,"IncrementZoom","DecrementZoom");    //For repetitive calls
        inputManager.addListener(actionListener,"Forward", "Backward", "Right", "Left", "Scan", "Play", "ClosePort","IncrementTurn","DecrementTurn","IncrementMove","DecrementMove"); //For single clicks
    }

    @Override
    public void simpleUpdate(float tpf) {
        updateTurn();
        updateMove();
        updateLine();

        //Displaying error connecting
        if(port.started == -1){
            updateStatus("Issue connecting. Please relaunch application");
        }

        //Adjusts bird's eye view if not correctly centered
        if(cam2.getLocation().getY() != camZoom){
            cam2.setLocation(cam2.getLocation().setY(camZoom));
        }

        //Disabling some default controls
        if(!reinitalizedInput) {
            //Disabling some default controls
            inputManager.deleteMapping("FLYCAM_Left");
            inputManager.deleteMapping("FLYCAM_Right");
            inputManager.deleteMapping("FLYCAM_Up");
            inputManager.deleteMapping("FLYCAM_Down");
            inputManager.deleteMapping("FLYCAM_ZoomIn");
            inputManager.deleteMapping("FLYCAM_ZoomOut");
            inputManager.deleteMapping("FLYCAM_StrafeLeft");
            inputManager.deleteMapping("FLYCAM_StrafeRight");
            inputManager.deleteMapping("FLYCAM_Forward");
            inputManager.deleteMapping("FLYCAM_Backward");

            inputManager.setCursorVisible(true);
            reinitalizedInput = true;
            setDisplayStatView(false); setDisplayFps(false);
        }

        //Toggles the sphere's color when the robot is busy
        if(port != null && port.started == 1 && !scanning && !turning && !backing && !moving){
            //Display the ready text to the user
            updateStatus("Ready!");
            Material sM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            sM.setColor("Color", ColorRGBA.Green);
            sG.setMaterial(sM);
        }else{
            Material sM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            sM.setColor("Color", ColorRGBA.Red);
            sG.setMaterial(sM);
        }

        //Running the necessary commands when the corresponding flag is set to true
        if(scanning){
            if(port.hasMsg()){
                String s = port.getString();
                scan(s);
            }
        }else if(moving){
            if(port.hasMsg()){
                moving();
            }
        }else if(turning){
            if(port.hasMsg()){
                turning();
            }
        }else if(backing){
            if(port.hasMsg()){
                backing();
            }
        }else{
            //Outputting any extra data that was received from the robot
            while(port.hasMsg()){
                System.out.println("Extra msgs caught: " + port.getString());
            }
        }

    }

    // ----------
    // Action Listeners
    // ----------

    /**
     * Action listener for all key inputs except zoom and executes the corresponding command if the robot is not already busy
     */
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (keyPressed && !scanning && port.started == 1 && !moving && !turning && !backing) {
                if (name.equals("Forward")) {
                    updateNotificaton("");
                    move();
                }
                if (name.equals("Backward")) {
                    back();
                }
                if (name.equals("Right")) {
                    turnR();
                }
                if (name.equals("Left")) {
                    turnL();
                }
                if (name.equals("Scan")){
                    updateNotificaton("");
                    scan();
                }
                if(name.equals("Play")){
                    String s = "P;";
                    updateStatus("Playing Music!!!");
                    completed = true;
                    updateSession("'"+"Completed"+"'");
                    port.send(s);
                }
                if(name.equals("ClosePort")){
                	if(!completed){
                		updateSession("'"+"Aborted"+"'");
                	}
                    try {
                        port.port.closePort();
                    } catch (SerialPortException e) {
                        e.printStackTrace();
                    }
                    while(portThread.isAlive()){
                    }
                }
                if(name.equals("IncrementTurn")){
                    incrementTurn(true);
                }
                if(name.equals("DecrementTurn")){
                    incrementTurn(false);
                }
                if(name.equals("IncrementMove")){
                    incrementMove(true);
                }
                if(name.equals("DecrementMove")){
                    incrementMove(false);
                }
            }
        }
    };

    /**
     * An action listener for the zoom buttons allowing for a fluid zoom in and out.
     */
    private AnalogListener analogListener = new AnalogListener() {
        public void onAnalog(String name, float value, float tpf) {
            if(name.equals("IncrementZoom")){
                incrementZoom(true);
            }
            if(name.equals("DecrementZoom")){
                incrementZoom(false);
            }
        }
    };

    // -----------
    // GUI Updates
    // -----------

    /**
     * Updates the status text
     * @param s String
     */
    private void updateStatus(String s){
        status.setText(s);
        status.setLocalTranslation((float)(cam.getWidth()/2 - status.getLineWidth()*.5), status.getLineHeight()+5, 0);
        updateStatusData(s);
    }

    /**
     * Updates the Turn text
     */
    private void updateTurn(){
        turnText.setText("Turn: " + turnValue + " Degrees");
        turnText.setLocalTranslation((float)(cam.getWidth()*0.85 - turnText.getLineWidth()*.5), turnText.getLineHeight()+5, 0);
    }

    /**
     * Updates the move text
     */
    private void updateMove(){
        moveText.setText("Move: " + moveValue/10 + " cm");
        moveText.setLocalTranslation((float)(cam.getWidth()*0.1 - moveText.getLineWidth()*.5), moveText.getLineHeight()+5, 0);
    }

    /**
     * Increments the turnValue based on whether it's the positive or negative button that was pressed (Max: 180, Min: 5, Increment: 5)
     * @param positive Boolean of whether to increment in positive or negative direction
     */
    private void incrementTurn(boolean positive){
        if (positive){
            if(turnValue != 180){
                turnValue+=5;
            }
        }else{
            if(turnValue != 5){
                turnValue -=5;
            }
        }
    }

    /**
     * Increments the moveValue based on whether it's the positive or negative button that was pressed (Max: 1000 mm, Min: 50 mm, Increment: 5)
     * @param positive Boolean of whether to increment in positive or negative direction
     */
    private void incrementMove(boolean positive){
        if (positive){
            if(moveValue != 1000){
                moveValue+=50;
            }
        }else{
            if(moveValue != 50){
                moveValue -=50;
            }
        }
    }

    /**
     * Increments the zoom height based on whether it's the positive or negative button that was pressed (Max: 1000, Min: 100, Increment: 1)
     * @param positive Boolean of whether to increment in positive or negative direction
     */
    private void incrementZoom(boolean positive){
        if(!positive){
            if(camZoom != 1000){
                camZoom+=1;
            }
        }else{
            if(camZoom != 100){
                camZoom -=1;
            }
        }
    }

    /**
     * Updates the notification text in the GUI
     * @param s String
     */
    private void updateNotificaton(String s){
        notification.setText(s);
        notification.setLocalTranslation((float)(cam.getWidth()/2 - notification.getLineWidth()*.5), notification.getLineHeight()*2.5f, 0);
    }

    /**
     * Updates the GUI to display the object detected and the sensor that detected it
     * @param s
     */
    private void dispObjDetected(String s){
        if(s.contains("B")) {
            updateNotificaton("Boulder "+s+" Detected");
        }

        else if(s.contains("C")) {
            updateNotificaton("Cliff "+s+" Detected");
        }
        else if(s.contains("L")) {
            Scanner scan = new Scanner(s);
            scan.next();
            updateNotificaton("Boundary "+scan.next()+" Detected");
        }

    }

    /**
     * Updates the white line that represents the distance the robot will travel is the move forward command is sent
     */
    private void updateLine(){
        rootNode.getChild("moveLine").removeFromParent();
        Vector3f current = roombaNode.getChild("dotNode").getWorldTranslation().clone().add(cam.getDirection().clone().normalize().mult(10f));
        Line line = new Line(current, current.add(cam.getDirection().clone().normalize().mult(moveValue/10.0f)));
        line.setLineWidth(5);
        Geometry lineG = new Geometry("moveLine", line);
        Material lineM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        lineM.setColor("Color", ColorRGBA.White);
        lineG.setMaterial(lineM);
        rootNode.attachChild(lineG);
    }

    /**
     * Removes any post that has not been given a permanent tag from the 3D space (yellow posts)
     */
    private void clearScanArea(){
        List<Spatial> children = rootNode.getChildren();
        for(int i = 0; i<children.size(); i++) {
            if(children.get(i).getName().equals("postNode")){
                Node node = (Node) children.get(i);
                Vector3f post = node.getChild("post").getWorldTranslation();
                double distance = post.distance(roombaNode.getChild("dotNode").getWorldTranslation());
                double between = Math.toDegrees(post.normalize().angleBetween(roombaNode.getChild("dotNode").getWorldTranslation().normalize()));
                if(between >= -90 && between <= 90 && distance<=60.0){
                    System.out.println("removed an object: Angle: " + between + " Distance: ");
                    children.get(i).removeFromParent();

                }
            }else{
                System.out.println("Unmatched object name: " + children.get(i).getName());
            }
        }
    }

    // ----------------
    // Movement Methods
    // ----------------

    /**
     * Moves the robot forwards in 3D space in the direction of the perspective
     * @param dist
     */
    public void moveForward(double dist){
        Vector3f previous = roombaNode.getWorldTranslation().clone().setY(0.5f);
        Vector3f v = cam.getDirection().clone().normalize().mult((float)dist);
        cam.setLocation(cam.getLocation().add(v));
        rootNode.getChild("roombaNode").move(v);
        cam2.setLocation(cam2.getLocation().add(v));

        Line line = new Line(previous, previous.add(v));
        line.setLineWidth(5);
        Geometry lineG = new Geometry("trail", line);
        Material lineM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        lineM.setColor("Color", ColorRGBA.Orange);
        lineG.setMaterial(lineM);
        rootNode.attachChild(lineG);
        Vector3f next = roombaNode.getWorldTranslation().clone().setY(0.5f);
        addTrail("trail_"+trailNum, "orange", previous, previous);
        trailNum++;
        updateViewData();
    }

    /**
     * Moves the robot backwards in 3D space in the opposite direction of the perspective
     * @param dist distance to travel
     */
    public void moveBackward(double dist){
        Vector3f previous = roombaNode.getWorldTranslation().clone().setY(0.5f);
        Vector3f v = cam.getDirection().clone().normalize().mult((float)-dist);
        cam.setLocation(cam.getLocation().add(v));
        rootNode.getChild("roombaNode").move(v);
        cam2.setLocation(cam2.getLocation().add(v));

        Line line = new Line(previous, previous.add(v));
        line.setLineWidth(5);
        Geometry lineG = new Geometry("trail", line);
        Material lineM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        lineM.setColor("Color", ColorRGBA.Brown);
        lineG.setMaterial(lineM);
        rootNode.attachChild(lineG);
        addTrail("trail_"+trailNum, "brown", previous, previous.add(v));
        trailNum++;
        updateViewData();
    }

    /**
     * Rotates the robot the given degrees clockwise
     * @param degree angle to rotate
     */
    public void rotateR(float degree){
        double theta = degree*(FastMath.PI * 2f/360f);
        Vector3f v = cam.getDirection().clone();
        double cs = Math.cos(theta);
        double sn = Math.sin(theta);
        rootNode.getChild("roombaNode").rotate(0f,-(float)theta,0f);

        v.setX((float)(v.getX()*cs - v.getZ()*sn));
        v.setZ((float)(v.getX()*sn + v.getZ()*cs));
        cam.lookAtDirection(v, new Vector3f(0,1,0));
        cam2.lookAtDirection(new Vector3f(0,-1,0), cam.getDirection().clone());
        updateLine();
    }

    /**
     * Rotates the robot the given degrees counterclockwise
     * @param degree angle to rotate
     */
    public void rotateL(float degree){
        float theta = degree*(FastMath.PI * 2f/360f);
        Vector3f v = cam.getDirection().clone();
        float cs = (float)Math.cos(-theta);
        float sn = (float)Math.sin(-theta);
        rootNode.getChild("roombaNode").rotate(0f,theta,0f);

        v.setX(v.getX()*cs - v.getZ()*sn);
        v.setZ(v.getX()*sn + v.getZ()*cs);
        cam.lookAtDirection(v, new Vector3f(0,1,0));
        cam2.lookAtDirection(new Vector3f(0,-1,0), cam.getDirection().clone());
        updateLine();

    }

    /**
     * Sends the scan command to the robot and sets the scanning flag to true as well as updates the user's status text
     */
    public void scan(){
        clearScanArea();
        String s = "S;";
        port.send(s);
        scanning = true;
        scanData = new ArrayList<String>();
        System.out.println("Starting scan");
        updateStatus("Scanning...");
    }

    /**
     * Parses the incoming data from the robot and properly presents it to the user in 3D space using a green line
     * @param data
     */
    public void scan(String data){

        Scanner sData = new Scanner(data);
        Integer degree = null;
        Float ir = null;
        Float sonar = null;
        scanData.add(data);

        if(sData.hasNextInt()){
            degree = sData.nextInt();
            if(sData.hasNextFloat()){
                ir = sData.nextFloat();
                if(sData.hasNextFloat()){
                    sonar = sData.nextFloat();
                }
            }
        }

        if(degree == 0) {
            Vector3f dotLoc = rootNode.getChild("dotNode").getWorldTranslation().clone().setY(3.8f);
            Vector3f view = cam.getDirection().clone().normalize();
            Line line = new Line(new Vector3f(0f, 0.5f, 0f), view.mult(80f).add(new Vector3f(0f, 0.5f, 0)));
            line.setLineWidth(5);
            Geometry lineG = new Geometry("line", line);
            Material lineM = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            lineM.setColor("Color", ColorRGBA.Green);
            lineG.setMaterial(lineM);
            Node lineNode = new Node("lineNode");
            lineNode.attachChild(lineG);
            lineNode.rotate(0f, (FastMath.PI * 2 / 360) * -90, 0f);
            lineNode.setLocalTranslation(dotLoc);
            rootNode.attachChild(lineNode);
        }else if (degree == 180){
            rootNode.getChild("lineNode").removeFromParent();
            degree = null;
            scanning = false;
            System.out.println("Ending scan");
            parseData();
        }else{
            rootNode.getChild("lineNode").rotate(0f, (FastMath.PI * 2/360), 0f);
        }
        previousDegree = degree;


    }

    /**
     * Sends the move forward command to the robot with the current moveValue, sets the moving flag to true and updates the GUI's status text
     */
    private void move(){
        String s = "F "+moveValue+" 150 100 1;";
        port.send(s);
        moving = true;
        updateStatus("Moving Forward...");
    }

    /**
     * Parses move forward data from the port and adjusts the 3D space as needed
     */
    private void moving(){
        if(port.hasMsg()){
            String msg = port.getString();
            if(msg.contains("D")){
                moving = false;
            }else if(checkObst(msg)){
                dispObjDetected(msg);
            }else{
                double dis = Double.valueOf(msg);
                moveForward(dis / 10);
            }
            updateRobotData();
        }
    }

    /**
     * Sends the necessary turn command to the robot including the turnValue, sets the turning flag and updates the GUI's status text (clockwise)
     */
    private void turnR(){
        String s = "R "+-turnValue+" 100;";
        port.send(s);
        turning = true;
        updateStatus("Turning Right...");
    }

    /**
     * Sends the necessary turn command to the robot including the turnValue, sets the turning flag and updates the GUI's status text (counterclockwise)
     */
    private void turnL(){
        String s = "R "+turnValue+" 100;";
        port.send(s);
        turning = true;
        updateStatus("Turning Left...");
    }

    private void turning(){
        if(port.hasMsg()){

            double calib = 0.95;
            String msg = port.getString();
            if(msg.contains("D")){
                System.out.println("Done turning");
                turning = false;
            }else{
                Scanner s = new Scanner(msg);
                double turn = s.nextDouble();
                if(turn > 0) {
                    rotateL((float)(turn*calib));
                }else{
                    rotateR((float)(-turn*calib));
                }
            }
            updateViewData();
        }
    }

    /**
     * Sends the reverse command to the robot for 10 cm and updates the GUI's status text
     */
    private void back(){
        String s = "B 100 100;";
        port.send(s);

        backing = true;
        updateStatus("Backing up...");
    }

    /**
     * Parses incoming backup data from the robot.
     */
    private void backing(){
        if(port.hasMsg()){
            String msg = port.getString();
            if(msg.contains("D")) {
                backing = false;
            }
            else{
                Scanner s = new Scanner(msg);
                int dist = s.nextInt();
                moveBackward(((double) -dist)/10);
            }
            updateRobotData();
        }
    }

    // ----------------
    // Data Analysis
    // ----------------

    /**
     * Parses a message from the robot to determine if an obstacle was detected. An appropriate object is then placed to represent the object.
     * @param s A String message form the robot
     * @return A boolean, true if an object was detected
     */
    private boolean checkObst(String s){
        boolean toReturn = false;
        Vector3f node = rootNode.getChild("roombaNode").getWorldTranslation().clone();

        if(s.contains("CL")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(180, 19, 5,5,"cliff");

        }else if(s.contains("CFL")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(100, 14, 5,5,"cliff");

        }else if(s.contains("CFR")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(80, 14, 5,5,"cliff");

        }else if(s.contains("CR")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(0, 19, 5,5,"cliff");

        }else if(s.contains("L 1")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(180, 19, 5,5,"boundary");

        }else if(s.contains("L 2")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(100, 14, 5,5,"boundary");

        }else if(s.contains("L 3")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(80, 14, 5,5,"boundary");

        }else if(s.contains("L 4")){
            moving = false;
            backing = true;
            toReturn = true;

            placeObj(0, 19, 5,5,"boundary");

        }else if(s.contains("BL")){
            moving = false;
            backing = true;
            toReturn = true;

            placeObj(135, 6, 13, 5, "boulder");

        }else if(s.contains("BR")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(45, 6, 13, 5, "boulder");

        }else if(s.contains("BB")){
            moving = false;
            backing = true;
            toReturn = true;
            placeObj(90, 5, 13, 5, "boulder");
        }

        return toReturn;
    }


    /**
     * Retrieves sweep data from the Port and converts them into IR and Sonar ArrayLists that are then analyzed to detect objects
     */
    private void parseData(){
        ArrayList<Double> ir = new ArrayList<Double>();
        ArrayList<Double> sonar = new ArrayList<Double>();

        for(String s: scanData){
            Scanner scanner = new Scanner(s);
            scanner.next();
            ir.add(scanner.nextDouble());
            sonar.add(scanner.nextDouble());
        }
        findObjs(ir, sonar);
    }

    /**
     * Analyzes IR and Sonar data from two ArrayLists to determine objects around the robot
     * @param ir An ArrayList of IR data
     * @param sonar An ArrayList of Sonar Data
     */
    private void findObjs(ArrayList<Double> ir, ArrayList<Double> sonar){
        boolean foundEdge = false;
        Integer startAngle = null;
        Integer endAngle = null;
        Double  startEdge= null;
        double range = 50.0;
        ArrayList<Integer> objAngles = new ArrayList<Integer>();
        ArrayList<Double> objDist = new ArrayList<Double>();
        ArrayList<Double> objWidth = new ArrayList<Double>();


        for(int i = 0; i<ir.size(); i++){
            Double num = ir.get(i);

            if(!foundEdge && num < range){
                startEdge = num;
                startAngle = i;
                foundEdge = true;
            }
            if(foundEdge && num > startEdge){
                endAngle = i;
                foundEdge = false;
                int angle = (int)Math.ceil((startAngle+endAngle)/2.0);
                if(angle >15 || angle < 165){
                    angle = angle - 11;
                }
                objAngles.add(angle);
                double distance = sonar.get((int)Math.ceil((startAngle+endAngle)/2.0))+1.5;
                objDist.add(distance);
                int width = endAngle - startAngle;
                objWidth.add((distance * Math.tan(Math.toRadians(width/2)))*2);
            }
        }
        if(foundEdge){
            endAngle = 180;
            if(((startAngle+endAngle)/2.0) >=15){
                objAngles.add((int)Math.ceil((startAngle+endAngle)/2.0)-15);
            }else{
                objAngles.add((int)Math.ceil((startAngle+endAngle)/2.0));
            }

            double distance = sonar.get((int)Math.ceil((startAngle+endAngle)/2.0));
            objDist.add(sonar.get((int)Math.ceil((startAngle+endAngle)/2.0)));
            int width = endAngle - startAngle;
            objWidth.add((distance * Math.tan(Math.toRadians(width/2)))*2);
        }

        for(int i = 0; i<objAngles.size(); i++){
            System.out.println("Found object: " + objAngles.get(i) + "  " + objDist.get(i) + "  " + objWidth.get(i));
            placeObj(objAngles.get(i), objDist.get(i), objWidth.get(i), 20, "post");
        }
    }

    /**
     * Places objects in the desegnated location relative to the robot
     * @param angle Angle of the object
     * @param dist  Distance awway from the robot
     * @param width Width of the object in cm
     * @param height  Height of the object
     * @param type  What type of object was detected (post, boulder, boundary, cliff)
     */
    private void placeObj(int angle, double dist, double width, double height, String type){
        boolean redDone = false;
        if(angle >=0 && angle <= 180 && width > 3.0 && dist <= 50.0) {
            if(type.equals("post")) {
                Cylinder b = new Cylinder(100, 100, (float) (width / 2.0), (float) height*2, true);
                if(width > 3.0 && width < 6.0){
                    Geometry geom = new Geometry("goal", b);  // create cube geometry from the shape
                    Material mat = new Material(assetManager,
                            "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
                        mat.setColor("Color", ColorRGBA.Green);//Probably a goal post

                    geom.setMaterial(mat);                   // set the cube's material
                    Node pivot = new Node("goal");
                    pivot.attachChild(geom);
                    geom.rotate((float) (3.14 / 2.0), 0, 0);
                    pivot.move(cam.getDirection().clone().normalize().mult((float) (dist + width)).setY((float) height));
                    Node pivot2 = new Node("goalNode");
                    pivot2.attachChild(pivot);

                    if (angle < 90) {
                        double rAngle = 90 - angle;
                        pivot2.rotate(0f, (float) -rAngle * (FastMath.PI * 2f / 360f), 0f);
                    } else if (angle > 90) {
                        double rAngle = angle - 90;
                        pivot2.rotate(0f, (float) rAngle * (FastMath.PI * 2f / 360f), 0f);
                    }
                    pivot2.move(rootNode.getChild("dotNode").getWorldTranslation().setY(0f));
                    rootNode.attachChild(pivot2);
                    addToDatabase(ObjectType.GoalPost, "green", pivot.getWorldTranslation().getX(),pivot.getWorldTranslation().getY(), pivot.getWorldTranslation().getZ(), (float)height, (float)(width/2.0));
                }else{
                    Geometry geom = new Geometry("post", b);  // create cube geometry from the shape
                    Material mat = new Material(assetManager,
                            "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
                    if (width < 6 && width > 3.0) {
                        mat.setColor("Color", ColorRGBA.Green);//Probably a goal post
                    }
                    else if (width < 7) {
                        mat.setColor("Color", ColorRGBA.Yellow);//Possibly a goal post
                    }
                    else {
                        mat.setColor("Color", ColorRGBA.Red);   //Not a goal post

                        geom = new Geometry("redPost", b);  // create cube geometry from the shape
                        geom.setMaterial(mat);                   // set the cube's material
                        Node pivot = new Node("redPost");
                        pivot.attachChild(geom);
                        geom.rotate((float) (3.14 / 2.0), 0, 0);
                        pivot.move(cam.getDirection().clone().normalize().mult((float) (dist + width)).setY((float) height));
                        Node pivot2 = new Node("redPostNode");
                        pivot2.attachChild(pivot);
                        if (angle < 90) {
                            double rAngle = 90 - angle;
                            pivot2.rotate(0f, (float) -rAngle * (FastMath.PI * 2f / 360f), 0f);
                        } else if (angle > 90) {
                            double rAngle = angle - 90;
                            pivot2.rotate(0f, (float) rAngle * (FastMath.PI * 2f / 360f), 0f);
                        }
                        pivot2.move(rootNode.getChild("dotNode").getWorldTranslation().setY(0f));
                        rootNode.attachChild(pivot2);
                        redDone = true;
                        addToDatabase(ObjectType.Post, "red", pivot.getWorldTranslation().getX(),pivot.getWorldTranslation().getY(), pivot.getWorldTranslation().getZ(), (float)height, (float)(width/2.0));
                    }

                    if(!redDone) {
                        geom.setMaterial(mat);                   // set the cube's material
                        Node pivot = new Node("post");
                        pivot.attachChild(geom);
                        geom.rotate((float) (3.14 / 2.0), 0, 0);
                        pivot.move(cam.getDirection().clone().normalize().mult((float) (dist + width)).setY((float) height));
                        Node pivot2 = new Node("postNode");
                        pivot2.attachChild(pivot);
                        if (angle < 90) {
                            double rAngle = 90 - angle;
                            pivot2.rotate(0f, (float) -rAngle * (FastMath.PI * 2f / 360f), 0f);
                        } else if (angle > 90) {
                            double rAngle = angle - 90;
                            pivot2.rotate(0f, (float) rAngle * (FastMath.PI * 2f / 360f), 0f);
                        }
                        pivot2.move(rootNode.getChild("dotNode").getWorldTranslation().setY(0f));
                        rootNode.attachChild(pivot2);
                        addToDatabase(ObjectType.Post, "red", pivot.getWorldTranslation().getX(),pivot.getWorldTranslation().getY(), pivot.getWorldTranslation().getZ(), (float)height, (float)(width/2.0));
                    }
                }

            }
            else if(type.equals("boulder")) {
                Cylinder b = new Cylinder(100, 100, (float) (width / 2.0), (float) height*2, true);
                Geometry geom = new Geometry("boulder", b);  // create cube geometry from the shape
                Material mat = new Material(assetManager,
                        "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
                if (width < 10) {
                    mat.setColor("Color", ColorRGBA.Green);//Probably a goal post
                } else if (width < 12) {
                    mat.setColor("Color", ColorRGBA.Yellow);//Possibly a goal post
                } else {
                    mat.setColor("Color", ColorRGBA.Red);   //Not a goal post
                }

                geom.setMaterial(mat);                   // set the cube's material
                Node pivot = new Node("boulder");
                pivot.attachChild(geom);
                geom.rotate((float) (3.14 / 2.0), 0, 0);
                pivot.move(cam.getDirection().clone().normalize().mult((float) (dist + width)).setY((float) height));
                Node pivot2 = new Node("boulderNode");
                pivot2.attachChild(pivot);
                if (angle < 90) {
                    double rAngle = 90 - angle;
                    pivot2.rotate(0f, (float) -rAngle * (FastMath.PI * 2f / 360f), 0f);
                } else if (angle > 90) {
                    double rAngle = angle - 90;
                    pivot2.rotate(0f, (float) rAngle * (FastMath.PI * 2f / 360f), 0f);
                }
                pivot2.move(rootNode.getChild("dotNode").getWorldTranslation().setY(0f));
                rootNode.attachChild(pivot2);
                addToDatabase(ObjectType.Boulder, null, pivot.getWorldTranslation().getX(),pivot.getWorldTranslation().getY(), pivot.getWorldTranslation().getZ(),0f, 0f);
            }
            else if(type.equals("boundary")){
                Sphere b = new Sphere(100, 100, 3f);
                Geometry geom = new Geometry("boundary", b);  // create cube geometry from the shape
                Material mat = new Material(assetManager,
                        "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
                mat.setColor("Color", ColorRGBA.Red);//Probably a goal post

                geom.setMaterial(mat);                   // set the cube's material
                Node pivot = new Node("objectNode");
                pivot.attachChild(geom);
                geom.rotate((float) (3.14 / 2.0), 0, 0);
                pivot.move(cam.getDirection().clone().normalize().mult((float) (dist)).setY(-1f));
                Node pivot2 = new Node("objectNode2");
                pivot2.attachChild(pivot);
                if (angle < 90) {
                    double rAngle = 90 - angle;
                    pivot2.rotate(0f, (float) -rAngle * (FastMath.PI * 2f / 360f), 0f);
                } else if (angle > 90) {
                    double rAngle = angle - 90;
                    pivot2.rotate(0f, (float) rAngle * (FastMath.PI * 2f / 360f), 0f);
                }
                pivot2.move(rootNode.getChild("dotNode").getWorldTranslation().setY(0f));
                rootNode.attachChild(pivot2);
                addToDatabase(ObjectType.Boundary, "red", pivot.getWorldTranslation().getX(),pivot.getWorldTranslation().getY(), pivot.getWorldTranslation().getZ(),0f, 0f);

            }
            else if(type.equals("cliff")){
                Sphere b = new Sphere(100, 100, 3f);
                Geometry geom = new Geometry("cliff", b);  // create cube geometry from the shape
                Material mat = new Material(assetManager,
                        "Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
                mat.setColor("Color", ColorRGBA.Blue);//Probably a goal post

                geom.setMaterial(mat);                   // set the cube's material
                Node pivot = new Node("objectNode");
                pivot.attachChild(geom);
                geom.rotate((float) (3.14 / 2.0), 0, 0);
                pivot.move(cam.getDirection().clone().normalize().mult((float) (dist)).setY(-1f));
                Node pivot2 = new Node("objectNode2");
                pivot2.attachChild(pivot);
                if (angle < 90) {
                    double rAngle = 90 - angle;
                    pivot2.rotate(0f, (float) -rAngle * (FastMath.PI * 2f / 360f), 0f);
                } else if (angle > 90) {
                    double rAngle = angle - 90;
                    pivot2.rotate(0f, (float) rAngle * (FastMath.PI * 2f / 360f), 0f);
                }
                pivot2.move(rootNode.getChild("dotNode").getWorldTranslation().setY(0f));
                rootNode.attachChild(pivot2);
                addToDatabase(ObjectType.Cliff, "blue", pivot.getWorldTranslation().getX(),pivot.getWorldTranslation().getY(), pivot.getWorldTranslation().getZ(),0f, 0f);
            }
        }
        updateLine();
    }
    
    // ----------------
    // Database Methods
    // ----------------
    
    /**
     * Creates the necessary tables for this session into the database
     */
    private void insertSessionTables(){
    	try {
			Class.forName("com.mysql.jdbc.Driver");
			String[] commands = {"CREATE TABLE "+sessionName+"_Posts"+" (Name text, Color text, x float, y float, z float, Height float, Radius float);",
					"CREATE TABLE "+sessionName+"_Trail"+" (Name text, Color text, x float, y float, z float);",
					"CREATE TABLE "+sessionName+"_Boulders"+" (Name text, x float, y float, z float);",
					"CREATE TABLE "+sessionName+"_Boundaries"+" (Name text, Color text, x float, y float, z float);",
					"CREATE TABLE "+sessionName+"_View"+" (Name text, xPos float, yPos float, zPos float, xVec float, yVec float, zVec float);",
					"CREATE TABLE "+sessionName+"_StatusText"+" (id int, Status text);"};
			for(int i = 0; i<commands.length; i++){
				PreparedStatement st = con.prepareStatement(commands[i]);
				st.executeUpdate();	
			}
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Inserts the session name and the value into the database
     * @param values the value to insert
     * @return the response
     */
    private int insertSessionData(String values){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO Sessions VALUES(" +"'" + sessionName + "','" + values + "'" + ");");
			int r1 = st.executeUpdate();
			
			return r1;
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
		return -1;
    }
    
    /**
     * Updates the session's status in the Session table
     * @param value the value to set to
     */
    private void updateSession(String value){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("UPDATE Sessions SET Status="+value + " WHERE Session=" + "'"+sessionName + "';");
			int r1 = st.executeUpdate();
			
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Adds any object from ObjectType to the database
     * @param type object type
     * @param color	objects color
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     * @param height objects height
     * @param radius objects radius
     */
    private void addToDatabase(ObjectType type, String color, float x, float y, float z, float height, float radius){
    	switch(type){
    	case GoalPost:
    		addPost("goalPost_"+goalPostNum,color, x, y, z, height, radius);
    		goalPostNum ++;
    		break;
    	
    	case Post:
    		addPost("post_"+postNum, color, x,y,z,height,radius);
    		postNum++;
    		break;
    	
    	case Boundary:
    		addBoundary("boundary_"+boundaryNum, color, x,y,z);
    		boundaryNum++;
    		break;
    	
    	case Cliff:
    		addBoundary("cliff_"+cliffNum, color, x,y,z);
    		cliffNum++;
    		break;
    		
    	case Boulder:
    		addBoulder("boulder_"+boulderNum, x,y,z);
    		boulderNum++;
    	}
    }
    
    /**
     * Adds a post to the database
     * @param name object's name
     * @param color	object's color
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     * @param height objects height
     * @param radius objects radius
     */
    private void addPost(String name, String color,  float x, float y, float z, float height, float radius){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO "+sessionName+"_Posts" +" VALUES(" +"'" + name + "','" + color + "'," +x + ","+y +"," + z +"," + height+ "," + radius+");");
			int r1 = st.executeUpdate();
			
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Adds a boundary object to the database
     * @param name object's name
     * @param color	object's color
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     */
    private void addBoundary(String name, String color, float x, float y, float z){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO "+sessionName+"_Boundaries" +" VALUES(" +"'" + name + "','" + color + "'," +x + ","+y +"," + z +");");
			int r1 = st.executeUpdate();
			
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Add a boulder object to the database
     * @param name object's name
     * @param x	x-axis location
     * @param y y-axis location
     * @param z z=axis location
     */
    private void addBoulder(String name, float x, float y, float z){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO "+sessionName+"_Boulders" +" VALUES(" +"'" + name + "'," +x + ","+y +"," + z +");");
			int r1 = st.executeUpdate();
			
			
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Adds a trail object to the databse
     * @param name object's name
     * @param color	object's color
     * @param v1 start vector
     * @param v2 end vector
     */
    private void addTrail(String name, String color, Vector3f v1, Vector3f v2){
    	try{
    		Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO "+sessionName+"_Trail" +" VALUES(" +"'" + name + "','" + color + "'," + v2.getX() + ","+v2.getY() +"," + v2.getZ()+");");
			int r1 = st.executeUpdate();
			
		
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Inserts the robot's location into the database
     */
    private void insertRobotData(){
    	Vector3f v1 = rootNode.getChild("roombaNode").getWorldTranslation();
    	try{
    		Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO "+sessionName+"_View" +" VALUES(" +"'" + "robot" + "'," +v1.getX() + ","+v1.getY() +"," + v1.getZ() +"," + 0f + ","+0f+"," + 0f+");");
			int r1 = st.executeUpdate();
			
		
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Updates robot data
     */
    private void updateRobotData(){
    	Vector3f v1 = rootNode.getChild("roombaNode").getWorldTranslation();
    	try{
    		Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("UPDATE "+sessionName+"_View SET xPos=" +v1.getX() + ",yPos="+v1.getY() +",zPos=" + v1.getZ() +",xVec=" + 0 + ",yVec="+0 +",zVec=" + 0+" WHERE Name='robot';");
			int r1 = st.executeUpdate();
			
		
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Inserts view data into the database
     */
    private void insertViewData(){
    	Vector3f v1 = cam.getLocation();
    	Vector3f v2 = cam.getDirection();
    	try{
    		Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO "+sessionName+"_View" +" VALUES(" +"'" + "view" + "'," +v1.getX() + ","+v1.getY() +"," + v1.getZ() +"," + v2.getX() + ","+v2.getY() +"," + v2.getZ()+");");
			int r1 = st.executeUpdate();
			
		
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Updates the view data
     */
    private void updateViewData(){
    	Vector3f v1 = cam.getLocation();
    	Vector3f v2 = cam.getDirection();
    	try{
    		Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("UPDATE "+sessionName+"_View SET xPos=" +v1.getX() + ",yPos="+v1.getY() +",zPos=" + v1.getZ() +",xVec=" + v2.getX() + ",yVec="+v2.getY() +",zVec=" + v2.getZ()+" WHERE Name='view';");
			int r1 = st.executeUpdate();
			
		
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * inserts the status data
     */
    private void insertStatusData(){
    	try{
    		Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("INSERT INTO "+sessionName+"_StatusText" +" VALUES(" +1+",'" + "Ready!" + "');");
			st.executeUpdate();
			
		
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Updates the current status data
     * @param str string to set as status in the database
     */
    private void updateStatusData(String str){
    	try{
    		Class.forName("com.mysql.jdbc.Driver");
			PreparedStatement st = con.prepareStatement("UPDATE "+sessionName+"_StatusText SET Status='" +str +"' WHERE id=1;");
			int r1 = st.executeUpdate();
			
		
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
    }
    
    /**
     * Represents the possible types of objects
     * @author Arun Sondhi
     *
     */
    public enum ObjectType {
    	Cliff, Boundary, Boulder, GoalPost, Post  
    }
    
    
}