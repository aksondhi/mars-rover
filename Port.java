/**
 * Created by aksondhi on 11/18/15.
 */

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.nio.charset.StandardCharsets;

/**
 * Represents a connection to the necessary Element Serial port
 */
public class Port implements SerialPortEventListener, Runnable {

    public SerialPort port;
    final ConcurrentLinkedDeque<String> messages = new ConcurrentLinkedDeque<String>();
    private volatile String currentMsg = "";
    protected int started = 0;

    public Port(String pathToPort) throws SerialPortException {
        port = new SerialPort(pathToPort);
    }

    public String getString() {
        synchronized (messages) {
            return messages.poll();
        }
    }

    public boolean hasMsg(){
        synchronized (messages) {
            return !messages.isEmpty();
        }
    }



    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
            if (serialPortEvent.isRXCHAR()) {
                try {
                    currentMsg += new String(port.readBytes(), "UTF-8");

                    while (currentMsg.indexOf(';') != -1) {
                        messages.add(currentMsg.substring(0, currentMsg.indexOf(';')));
                        currentMsg = currentMsg.substring(currentMsg.indexOf(';') + 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }

    /**
     * Initializes connection
     */
    @Override
    public void run() {
        for(int i = 1; i<3; i++) {
            if(started != 1) {
                try {
                    port.openPort();
                    port.setParams(SerialPort.BAUDRATE_57600, SerialPort.DATABITS_8, SerialPort.STOPBITS_2, SerialPort.PARITY_NONE);
                    port.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
                    port.setEventsMask(SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR);
                    port.addEventListener(this);
                    System.out.println("started");
                    started = 1;
                    
                } catch (SerialPortException e) {
                    System.out.println("Unable to connect. Attempt: " + i);
                }
            }
        }
        if(started != 1){
            started = -1;
        }

    }

    /**
     * Sends the given string to the connection
     * @param s String
     */
    public void send(String s){
//        byte bytes[] = s.getBytes(StandardCharsets.UTF_8);
//        for(int i = 0; i<bytes.length; i++){
//            try {
//                port.writeByte(bytes[i]);
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            } catch (SerialPortException e) {
//                e.printStackTrace();
//            }
//        }
        //Another faster way of sending commands
        try {
            port.writeString(s, "UTF-8");
        } catch (SerialPortException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
